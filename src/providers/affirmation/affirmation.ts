import { Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Observable } from 'rxjs/Observable';
import { Cache } from 'ionic-cache-observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/finally';
import { Affirmation } from './affirmation.model';

@Injectable()
export class AffirmationProvider {

  private API_URL = "http://affirmations.digitalfractaltechnologies.com/affirmations.json";
  public cache: Cache<Affirmation[]>;
  public pinnedCnt$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public myAffirmationCnt$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public affirmationCnt$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public categoriesCnt$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public affirmations$: BehaviorSubject<Affirmation[]> = new BehaviorSubject<Affirmation[]>([]);
  public affirmationCategories$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public isLoading: boolean = false;
  public isError: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(
    private httpClient: HttpClient,
    private http: HTTP,
    private platform: Platform,
  ) {}

  fetchData(): Observable < Affirmation[] > {
    this.isLoading = true;
    if (this.platform.is('cordova')) {
        return Observable.fromPromise(
          this.http.get(this.API_URL, {}, {}).then((res) => {
          if (!res.error) {
            this.isLoading = false;
            return (JSON.parse(res.data));
          }
        })
      );
    } else {
      return this.httpClient.get < Affirmation[] > (this.API_URL).map((c)=>{
        this.isLoading = false;
        return c;
      });
    }
  }

  public getByAffirmation(affirmations:Affirmation[], text:string){
    const aff = affirmations;
    if(text){
      const that = this;
      return aff.filter(function(v){
        return v.AFFIRMATION.toLowerCase().match(new RegExp(that.RegExp_escape(text.toLowerCase())));
      });
    }else{
      return affirmations;
    }
  }

  public getByCategory(affirmations:Affirmation[], cat: string){
    return affirmations.filter(v => {
      return v.CATEGORIES.indexOf(cat) > -1;
    });
  }

  private RegExp_escape(s: string) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  };
}
