export interface Affirmation {
  ID: string,
  AFFIRMATION: string,
  CATEGORIES: Array<string>,
  pinned?: boolean,
  myaffirmation?: boolean
}
