import { Affirmation } from './../../providers/affirmation/affirmation.model';
import { AffirmationProvider } from './../../providers/affirmation/affirmation';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController, Refresher, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs/Subscription';
import { Cache, CacheService } from 'ionic-cache-observable';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  datas: Affirmation[];
  originalDatas: Affirmation[];
  the_category: string;
  searchInput: string;
  pinned: Array<any>;
  cc: Subscription;
  isLoaded: boolean = false;
  private myAffirmationData:Affirmation[]=[];
  private cacheSubcription: Subscription;
  private errSubsc: Subscription;
  private refresher: Refresher;
  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private affirmationProvider: AffirmationProvider,
    private cacheService: CacheService
  ) {
    this.datas = [];
    this.originalDatas = [];
    this.the_category = "";
    this.searchInput = "";
    this.pinned = [];
    this.cc = null;
    this.isLoaded = false;
    this.getData();
  }

  doRefresh(refresher: Refresher) {
    this.refresher = refresher;
    this.cacheSubcription = this.cacheService.get('affirmations')
      .subscribe((cache: Cache < Affirmation[] > ) => {
        cache.refresh().subscribe();
        this.checkPinned();
        refresher.complete();
        this.refresher = null;
      })
  }

  private async checkPinned() {
    const that = this;
    const dt = that.originalDatas;
    that.myAffirmationData = [];
    that.the_category = "";
    Promise.all([
      that.storage.get("myaffirmation"),
      that.storage.get('pinned')
    ]).then((storageValue)=>{
      this.affirmationProvider.affirmationCnt$.next(dt.length);
      that.pinned = JSON.parse(storageValue[1]) || that.pinned;
      that.myAffirmationData = storageValue[0] || that.myAffirmationData;
      that.affirmationProvider.myAffirmationCnt$.next(that.myAffirmationData.length);
      const myaffirmation = this.myAffirmationData.map(function (v) {
        v['pinned'] = that.pinned.indexOf(v.ID) > -1;
        return v;
      });
      myaffirmation.reverse();
      const remotedt:Affirmation[] = dt.map(v => {
        v['myaffirmation'] = false;
        v['pinned'] = that.pinned.indexOf(v.ID) > -1;
        return v;
      });
      that.datas = null;
      that.datas = myaffirmation.concat(remotedt);

      const tabs = this.navCtrl.parent;
        if (tabs.viewCtrl.data.cat) {
          this.the_category = tabs.viewCtrl.data.cat;
          this.filterCategory(this.the_category);
        }
      if(!that.isLoaded)
        that.isLoaded = true;
    }).catch(()=>{
      if(!that.isLoaded)
        that.isLoaded = true;
    })
  }

  search(ev: any) {
    const val = ev.target.value;

    if (this.the_category) {
      this.filterCategory(this.the_category);
    }else{
      this.datas = this.myAffirmationData.concat(this.originalDatas);
    }
    if (val && val.trim() != '') {
      this.datas = this.affirmationProvider.getByAffirmation(this.datas, val);
    } else {
      this.searchInput = "";
    }
  }

  filterCategory(category: string) {
    const dt = this.myAffirmationData.concat(this.originalDatas);
    this.the_category = category;
      if(this.the_category == 'My Affirmation'){
        this.datas = dt.filter((v)=>{
          return v.myaffirmation == true;
        })
      }else{
        this.datas = this.affirmationProvider.getByCategory(dt, this.the_category);
      }
  }

  pinme(data: any) {
    const that = this;
    const id = data.ID;
    if (that.pinned && that.pinned.indexOf(id) == -1) {
      that.pinned.push(id);
      that.storage.set('pinned', JSON.stringify(that.pinned)).then(function () {
        that.affirmationProvider.pinnedCnt$.next(that.pinned.length);
        that.datas[that.datas.indexOf(data)].pinned = true;
        let toast = that.toastCtrl.create({
          message: 'Affirmation pinned successfully',
          duration: 3000,
          position: 'top'
        });

        toast.present();
      });
    } else {
      this.showUnpinConfirm(data);
    }
  }

  showUnpinConfirm(data: any) {
    const that = this;
    const confirm = that.alertCtrl.create({
      title: 'Unpin Affirmation?',
      message: 'Are you sure to unpin this affirmation?',
      buttons: [{
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
            that.unpinme(data);
          }
        }
      ]
    });
    confirm.present();
  }

  unpinme(data: any) {
    const that = this;
    const id = data.ID;
    that.pinned.splice(that.pinned.indexOf(id), 1);
    that.storage.set('pinned', JSON.stringify(that.pinned)).then(function () {
      that.affirmationProvider.pinnedCnt$.next(that.pinned.length);
      that.datas[that.datas.indexOf(data)].pinned = false;
      let toast = that.toastCtrl.create({
        message: 'Affirmation unpinned successfully',
        duration: 3000,
        position: 'top'
      });

      toast.present();
    });
  }

  showDeleteConfirm(id: any) {
    const that = this;
    const confirm = that.alertCtrl.create({
      title: 'Delete Affirmation?',
      message: 'Are you sure to delete this affirmation?',
      buttons: [{
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
            that.removeMyAffirmation(id);
          }
        }
      ]
    });
    confirm.present();
  }

  removeMyAffirmation(id){
    const that = this;
    that.storage.get('myaffirmation').then(function(affirmations: Affirmation[]){
      affirmations.splice(affirmations.map((dt)=>dt.ID).indexOf(id), 1);
      return affirmations;
    })
    .then(function(affirmations){
      that.storage.set('myaffirmation', affirmations).then(function(){
        let toast = that.toastCtrl.create({
          message: 'Affirmation deleted successfully',
          duration: 3000,
          position: 'top'
        });
        toast.present();
        that.checkPinned();
      })
    })
  }

  showForm(){
    const modal = this.modalCtrl.create("MyaffirmationformPage", this.originalDatas);
    modal.onDidDismiss((dismissed) => {
      if(dismissed == false)
        this.checkPinned();
    })
    modal.present();
  }

  resetFilter() {
    this.the_category = "";
    this.searchInput = "";
    const tabs = this.navCtrl.parent;
    if(tabs.viewCtrl.data.cat)
      tabs.viewCtrl.data = {};

    this.checkPinned();
  }

  getData() {
    // this.isLoaded = false;
    this.cc = this.affirmationProvider.affirmations$
    // .filter(data => !!data)
    .subscribe((affirmationList) => {

      this.originalDatas= affirmationList;
      this.datas = this.originalDatas;
      this.checkPinned();
    })
  }

  ionViewWillEnter(){
    this.isLoaded = false;
    // this.isOffline = false;
    // const affirmationObservable: Observable<Affirmation[]> = this.affirmationProvider.fetchData();
    // this.cc = this.cacheService.register('affirmations', affirmationObservable)
    //   .mergeMap((cache: Cache<Affirmation[]>) => {
    //     this.cache = cache;
    //     this.affirmationProvider.cache = this.cache;
    //     this.cacheSubcription = this.cache.refresh().subscribe();
    //     return this.cache.data$(()=>{
    //       this.isOffline = true;
    //     });
    //   }).subscribe((dt) => {
    //     console.log('updatede');
    //     console.log(this.the_category);
    //     this.originalDatas = dt;
    //     this.datas = this.originalDatas;
    //     this.checkPinned();
    //   })
    this.errSubsc = this.affirmationProvider.isError.subscribe(()=>{
      if(this.refresher){
        this.refresher.cancel();
        this.refresher = null;
      }
    })
    this.checkPinned();
  }

  ionViewWillLeave() {
    this.errSubsc.unsubscribe();
    if(this.cacheSubcription)
      this.cacheSubcription.unsubscribe();
    this.cc.unsubscribe();
  }
}
