import { Affirmation } from './../../providers/affirmation/affirmation.model';
import { Component } from '@angular/core';
import { NavController, IonicPage, ToastController, AlertController, Refresher, FabContainer } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AffirmationProvider } from './../../providers/affirmation/affirmation';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import { Subscription } from 'rxjs/Subscription';
import { Cache, CacheService } from 'ionic-cache-observable';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private res: Affirmation[];
  datas: Affirmation[];
  private originalData: Affirmation[];
  pinned: Array < any > ;
  category: string;
  categories: Array < any > ;
  isLoaded: boolean;
  isOffline: boolean;
  private cc: Subscription;
  private cacheSubcription: Subscription;
  private refresher: Refresher;
  private errSubsc: Subscription;
  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private affirmationProvider: AffirmationProvider,
    private cacheService: CacheService
  ) {
    this.res = [];
    this.datas = [];
    this.originalData = [];
    this.pinned = [];
    this.category = "all";
    this.categories = [];
    this.isLoaded = false;
    this.isOffline = false;
    this.getData();
  }

  getPinnedAffirmation() {
    const that = this;
    // that.res = [];
    // that.pinned = [];
    that.datas = [];
    // that.categories = [];
    Promise.all([
      that.storage.get('pinned'),
      that.storage.get("myaffirmation")
    ]).then((storageValue) => {
      that.pinned = JSON.parse(storageValue[0]) || that.pinned;
      if (that.pinned.length > 0) {
        that.affirmationProvider.pinnedCnt$.next(that.pinned.length);
        const remoteData = that.originalData.map(function (v) {
          v['myaffirmation'] = false;
          return v;
        }).filter(function (v) {
          return that.pinned.indexOf(v.ID) > -1;
        });
        const myaffirmation = storageValue[1] || [];
        let localData: any[] = [];
        if (myaffirmation.length > 0) {
          that.affirmationProvider.myAffirmationCnt$.next(myaffirmation.length);
          localData = myaffirmation.map(function (v) {
            v['myaffirmation'] = true;
            return v;
          }).filter(function (v) {
            return that.pinned.indexOf(v.ID) > -1;
          });
        }
        const tempData = remoteData.concat(localData);
        that.res = that.shuffle(tempData);
        tempData.forEach(function (v) {
          v.CATEGORIES.forEach(function (d) {
            if (that.categories.indexOf(d) < 0) {
              that.categories.push(d);
            }
          });
        });
        that.filterPinned();
      } else {
        that.isLoaded = true;
      }
    }).catch(()=>{
      // console.log(err);
    })
  }

  filterPinned(cat ? : string) {
    const that = this;
    that.category = cat || that.category;
    if (that.category == "all") {
      that.datas = that.res;
    } else {
      that.datas = that.res.filter(function (v) {
        return v.CATEGORIES.indexOf(that.category) > -1;
      });
    }
    that.isLoaded = true;
  }

  shuffle(array) {
    var currentIndex = array.length,
      temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  showRadio() {
    const that = this;
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'radio',
      label: 'All',
      value: 'all',
      checked: this.category == "all"
    });
    this.categories.forEach(function (v) {
      alert.addInput({
        type: 'radio',
        label: v.toUpperCase(),
        value: v,
        checked: that.category == v
      });
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.category = data;
        this.filterPinned();
      }
    });
    alert.present();
  }

  showConfirm(id) {
    const that = this;
    const confirm = that.alertCtrl.create({
      title: 'Unpin Affrimation?',
      message: 'Are you sure to unpin this affirmation?',
      buttons: [{
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
            that.unpinme(id);
          }
        }
      ]
    });
    confirm.present();
  }

  goSearch(search ? : string, fab?: FabContainer) {
    const tabs = this.navCtrl.parent;
    tabs.viewCtrl.data = {};
    if (search)
      tabs.viewCtrl.data['cat'] = search;
    if (fab)
      fab.close();

    tabs.select(1);
  }

  unpinme(id) {
    const that = this;
    that.pinned.splice(that.pinned.indexOf(id), 1);
    that.storage.set('pinned', JSON.stringify(that.pinned)).then(function () {
      that.affirmationProvider.pinnedCnt$.next(that.pinned.length);
      that.getPinnedAffirmation();
      let toast = that.toastCtrl.create({
        message: 'Affirmation unpinned successfully',
        duration: 3000,
        position: 'top'
      });

      toast.present();
    });
  }

  exploreCategory(cat: string) {
    const tabs = this.navCtrl.parent;
    tabs.viewCtrl.data['cat'] = cat.toLowerCase();
    tabs.select(1);
  }

  about() {
    this.navCtrl.push('AboutPage');
  }

  doRefresh(refresher?: Refresher) {
    this.refresher = refresher;
    this.cacheSubcription = this.cacheService.get('affirmations')
      .subscribe((cache: Cache < Affirmation[] > ) => {
        cache.refresh().subscribe();
        this.getPinnedAffirmation();
        refresher.complete();
        this.refresher = null;
      })
  }

  getData() {
    this.isLoaded = false;
    this.cc = this.affirmationProvider.affirmations$
    // .filter(data => !!data)
    .subscribe((affirmationList) => {
      this.originalData = affirmationList;
    })
  }

  ionViewWillEnter(){
    this.errSubsc = this.affirmationProvider.isError.subscribe(()=>{
      if(this.refresher){
        this.refresher.cancel();
        this.refresher = null;
      }
    })
    this.getPinnedAffirmation();
  }

  ionViewWillLeave() {
    this.errSubsc.unsubscribe();
    if(this.cacheSubcription)
      this.cacheSubcription.unsubscribe();
    this.cc.unsubscribe();
  }

}
