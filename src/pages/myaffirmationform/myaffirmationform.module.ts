import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyaffirmationformPage } from './myaffirmationform';

@NgModule({
  declarations: [
    MyaffirmationformPage,
  ],
  imports: [
    IonicPageModule.forChild(MyaffirmationformPage),
  ],
})
export class MyaffirmationformPageModule {}
