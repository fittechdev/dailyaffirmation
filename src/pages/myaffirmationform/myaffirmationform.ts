import { Subscription } from 'rxjs/Subscription';
import { AffirmationProvider } from './../../providers/affirmation/affirmation';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Alert, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-myaffirmationform',
  templateUrl: 'myaffirmationform.html',
})
export class MyaffirmationformPage {
  categories = [];
  category: any[];
  affirmation: string;
  cc: Subscription;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private storage: Storage,
    private alertCtrl: AlertController,
    private affirmationProvider: AffirmationProvider
  ) {  }

  async addAffirmation() {
    if ((this.affirmation && this.affirmation.length > 0) && (this.category && this.category.length > 0)) {
      const that = this;
      try {
        let data: Array<any> = await that.storage.get("myaffirmation") || [];
        const affirm = {
          ID: "myaffirmation_" + that.generate(),
          AFFIRMATION: that.affirmation,
          CATEGORIES: that.category,
          myaffirmation: true
        }
        data.push(affirm);
        await that.storage.set("myaffirmation", data);
        that.affirmationProvider.myAffirmationCnt$.next(data.length);
        that.closeForm(false);
      } catch (error) {
        console.log(error);
      }
    } else {
      const alert: Alert = this.alertCtrl.create({
        title: 'Warning',
        message: 'Please input the field correctly.',
        buttons: ["Ok"]
      });
      alert.present();
    }
  }

  generate() {
    const timestamp = +new Date;
    const length = 8;
    var ts = timestamp.toString();
    var parts = ts.split("").reverse();
    var id = "";

    for (var i = 0; i < length; ++i) {
      var index = Math.floor(Math.random() * (length - 1 - 0 + 1)) + 0;
      id += parts[index];
    }
    return id;
  }

  closeForm(dismiss:boolean = false) {
    this.viewCtrl.dismiss(dismiss);
  }

  ionViewWillEnter() {
    const that = this;
      this.viewCtrl.data.forEach(function(v){
        v.CATEGORIES.forEach(function(d: any){
          if(that.categories.indexOf(d)<0){
            that.categories.push(d);
          }
        });
      })
  }
}
