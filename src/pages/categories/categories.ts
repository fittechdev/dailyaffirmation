import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { AffirmationProvider } from './../../providers/affirmation/affirmation';

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  categories = [];
  cc:Subscription = null;
  isOffline: boolean;
  constructor(
    public navCtrl: NavController,
    private affirmationProvider: AffirmationProvider,
  ) {
    this.categories = [];
    this.isOffline = false;
  }

  exploreCategory(cat: string) {
    const tabs = this.navCtrl.parent;
    tabs.viewCtrl.data['cat'] = cat.toLowerCase();
    tabs.select(1);
  }

  ionViewWillEnter(){
    this.cc = this.affirmationProvider.affirmationCategories$.subscribe((categories) => {
      this.categories = categories;
    })
  }

  ionViewWillLeave() {
    this.cc.unsubscribe();
    // this.affirmationProvider.unsubscribeAffirmation();
  }

}
