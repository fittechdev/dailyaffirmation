import { Subscription } from 'rxjs/Subscription';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AffirmationProvider } from '../providers/affirmation/affirmation';
import { CacheService, Cache } from 'ionic-cache-observable';
import { Affirmation } from '../providers/affirmation/affirmation.model';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'TabsPage';
  @ViewChild(Nav) nav: Nav;
  public pinnedCnt: number;
  public myAffirmationCnt: number;
  public affirmationCnt: number;
  public categoriesnCnt: number;
  private pinnedCntSubsc: Subscription;
  private myAffirmationCntSubsc: Subscription;
  private affirmationCntSubsc: Subscription;
  private categoriesnCntSubsc: Subscription;
  private cacheSubsc: Subscription;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private affirmationProvider: AffirmationProvider,
    private cacheService: CacheService
    ) {
    platform.ready().then(() => {
      statusBar.styleBlackTranslucent();
      splashScreen.hide();
      this.pinnedCntSubsc = this.affirmationProvider.pinnedCnt$.subscribe((v)=>{
        this.pinnedCnt = v;
      });

      this.myAffirmationCntSubsc = this.affirmationProvider.myAffirmationCnt$.subscribe((v)=>{
        this.myAffirmationCnt = v;
      });

      this.affirmationCntSubsc = this.affirmationProvider.affirmationCnt$.subscribe((v)=>{
        this.affirmationCnt = v;
      });

      this.categoriesnCntSubsc = this.affirmationProvider.categoriesCnt$.subscribe((v)=>{
        this.categoriesnCnt = v;
      });
      const affirmationObservable = this.affirmationProvider.fetchData();
      this.cacheSubsc = this.cacheService.register('affirmations', affirmationObservable)
      .mergeMap((cache: Cache<Affirmation[]>) => {
        this.affirmationProvider.isError.next(false);
        return cache.data$(()=>{
          // console.log(err);
          this.affirmationProvider.isError.next(true);
        })
      })
      .subscribe((data)=>{
        this.affirmationProvider.affirmations$.next(data);
        this.affirmationProvider.affirmationCnt$.next(data.length);
        let categories = [];
        data.forEach(function (v) {
        v.CATEGORIES.forEach(function (d: any) {
          if (categories.indexOf(d) < 0) {
            categories.push(d);
          }
        });
      })
      this.affirmationProvider.affirmationCategories$.next(categories);
      this.affirmationProvider.categoriesCnt$.next(categories.length);
      }, () => {
        // console.log('err', err);
      })
    });
  }

  OnDestroy(){
    this.pinnedCntSubsc.unsubscribe();
    this.myAffirmationCntSubsc.unsubscribe();
    this.affirmationCntSubsc.unsubscribe();
    this.categoriesnCntSubsc.unsubscribe();
    this.cacheSubsc.unsubscribe();
  }

  openPage(id: any, all?:boolean){
    if(id<3){
      this.nav.getActiveChildNavs()[0].viewCtrl.data = {};
      if(id==1){
        if(!all)
          this.nav.getActiveChildNavs()[0].viewCtrl.data['cat'] = 'My Affirmation';
      }
      if(this.nav.getActiveChildNavs()[0].getSelected().index == id){
        this.affirmationProvider.cache.refresh().subscribe();
      }else{
        this.nav.getActiveChildNavs()[0].select(id);
      }
    }else{
      if(id==3)
        this.nav.push("AboutPage");
    }
    if(this.menuCtrl.isOpen()){
      this.menuCtrl.close();
    }
  }
}
